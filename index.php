<?php


use Core\Database;
use Core\Router;
use Core\View;
use Core\connexionDB;

// TODO vérifier s'il y a d'autres controllers à appeler
use \App\Controllers\PageController;
use \App\Controllers\UserController;

// Paramètres Base de données
define( 'DB_HOST', 'localhost' );
define( 'DB_NAME', 'rbnb' );
define( 'DB_USER', 'root' );
define( 'DB_PASS', '' );

// Paramètres PDO
define( 'PDO_ENGINE', 'mysql' );
define( 'PDO_OPTIONS', [
	PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
]);

// Chemins de fichiers
define( 'DS', DIRECTORY_SEPARATOR );
define( 'ROOT_PATH', dirname(__FILE__) . DS );

spl_autoload_register();

session_start();

require_once ROOT_PATH.'vendor'.DS.'autoload.php';

$router = new \MiladRahimi\PhpRouter\Router();

// le paramètre "controller" contient "\App\Controllers\PageController@index"
$router
	->get( '/', PageController::class . '@index' )
	->get( '/', UserController::class . '@connexionProcess')
    ->get( '/users', UserController::class . '@index' )
    ->get( '/inscription', UserController::class. '@signin')
    ->post( '/inscription', UserController::class. '@signinProcess')
	->get( '/users/{user_id}', UserController::class . '@show')
	->get( '/contact', PageController::class . '@contactFormDisplay')
	->post( '/contact', PageController::class . '@contactFormProcess')
	->get('/reservation', UserController::class . '@resa')
	->post('/reservation', UserController::class . '@resa')
	->get( '/chambre/{chambre_id}', UserController::class . '@show')
	->get('/userListe', UserController::class . '@userListe')
	->get('/connexion', UserController::class . '@connexion')
	->post('/connexion', UserController::class . '@connexionProcess')
	->get('/ajoutLocation', UserController::class . '@formLoc')
	->post('/ajoutLocation', UserController::class . '@ajoutLocation');

// Démarrage du routeur
try {
	// Essai de lancement
	$router->dispatch();
}
// Si erreur 404
catch( \MiladRahimi\PhpRouter\Exceptions\RouteNotFoundException $exception )
{
	$view = new View( 'error-404' );
	$view->get404();
}
// Si erreur sur l'appel du controller
catch( \MiladRahimi\PhpRouter\Exceptions\InvalidControllerException $exception ) {
	echo 'InvalidControllerException';
}