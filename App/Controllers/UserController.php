<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;

use Zend\Diactoros\ServerRequest;

class UserController extends Controller
{
	public function index(): void
	{
		$view = new View( 'user-list' );

		$view_data = [
			'html_title' => 'Liste des utilisateurs',
			'html_h1' => 'Les utilisateurs',
			'users' => $this->rm->getUserRepo()->findAll()
		];

		$view->render( $view_data );
	}

	public function signin(): void
	{
		// S'il y a une session alors on ne retourne plus sur cette page
		if (isset($_SESSION['id'])){
			header('Location: /');
			exit;
		}

		$view = new View('inscription');

		$view_data = [
			'html_title' => 'inscription',
			'html_h1' => 'inscription'
		];


		$view->render( $view_data );
	}

	public function signinProcess(ServerRequest $request): void
	{
		$form_data = $request->getParsedBody();

		// Si la variable "$_Post" contient des informations alors on les traitres
		$valid = true;

		// On se place sur le bon formulaire grâce au "name" de la balise "input"


		//  Vérification du nom
		if(empty($form_data['nom'])){
			$valid = false;
			$er_nom = ("Le nom d' utilisateur ne peut pas être vide");
		}

		//  Vérification du prénom
		if(empty($form_data['prenom'])){
			$valid = false;
			$er_prenom = ("Le prenom d' utilisateur ne peut pas être vide");
		}

		// Vérification du mail
		if(empty($form_data['mail'])){
			$valid = false;
			$er_mail = "Le mail ne peut pas être vide";

			// On vérifit que le mail est dans le bon format
		}
		else if(!preg_match("/^[a-z0-9\-_.]+@[a-z]+\.[a-z]{2,3}$/i", $form_data['mail'])) {
			$valid = false;
			$er_mail = "Le mail n'est pas valide";
		}
/*
		if ($req_mail['mail'] != ""){
				$valid = false;
				$er_mail = "Ce mail existe déjà";
		}
*/

		// Vérification du mot de passe
		if(empty($form_data['mdp'])) {
			$valid = false;
			$er_mdp = "Le mot de passe ne peut pas être vide";

		}elseif($form_data['mdp'] != $form_data['confmdp']){
			$valid = false;
			$er_mdp = "La confirmation du mot de passe ne correspond pas";
		}

		// Si toutes les conditions sont remplies alors on fait le traitement
		if($valid = true){
			$form_data['mdp'] = hash('sha512', $form_data['mdp']);

			$user = $this->rm->getUserRepo()->PostLogin($form_data);

			header('Location: /');

			$_SESSION['id'] = $user->id;
			$_SESSION['nom'] = $user->nom;
			$_SESSION['prenom'] = $user->prenom;
			$_SESSION['mail'] = $user->mail;
			$_SESSION['tel'] = $user->tel;
			$_SESSION['role'] = $user->role;

		} else {
			echo "Inscription échouée !";

		}

					//  $verif_mail = $db->query("ERROR 1062");

		//header('Location: /');
		exit;

	}


	public function connexion(): void
	{

		// S'il y a une session alors on ne retourne plus sur cette page
		if (isset($_SESSION['id'])){
			header('Location: /');
			exit;
		}

			$view = new View('connexion');

			$view_data = [
				'html_title' => 'connexion',
				'html_h1' => 'connexion'
			];

			$view->render( $view_data );
	}

	public function connexionProcess(ServerRequest $request): void
	{
		$post_data = $request->getParsedBody();

		if(isset($post_data['mail']) && isset($post_data['mdp']))
		{
			$mail =  $post_data['mail'];
			$mdp = $post_data['mdp'];
			if(!empty($mail) && !empty($mdp))
			{
				$user = $this->rm->getUserRepo()->checkAuth($mail, $mdp);

				if(!is_null($user)) // nom d'utilisateur et mot de passe corrects
				{
					$_SESSION['id'] = $user->id;
					$_SESSION['nom'] = $user->nom;
					$_SESSION['prenom'] = $user->prenom;
					$_SESSION['mail'] = $user->mail;
					$_SESSION['tel'] = $user->tel;
					$_SESSION['role'] = $user->role;

					header('Location: /');
				}
				else
				{
					header('Location: /connexion?erreur=1'); // utilisateur ou mot de passe incorrect
				}
			}
			else
			{
				header('Location: /connexion?erreur=2'); // utilisateur ou mot de passe vide
			}
		}
		else {
			header('Location: /connexion?erreur=3'); // Les champs ne sont pas dasn le formulaire
		}

		exit;
	}

	public function userListe(ServerRequest $request): void
	{
		$post_data = $request->getParsedBody();

		$view = new View('userListe');

		$view_data = [
			'html_title' => 'userListe',
			'html_h1' => 'Liste des clients et des réservations',
			'user' => $this->rm->getUserRepo()->findAll(),
		];

		$view->render( $view_data );
	}



	public function resa() {
		$chambre = $chambres->id; // ok alors c'est bien mignon, mais comment je fais le lien entre
        // la chambre dont le bouton à été cliqué et n'importe quelle autre chambre ?
        $client = $_SESSION['id'];

            if(isset($date_de_debut) && isset($date_de_fin)){
                $query = Database::get()->prepare('INSERT INTO reservation (chambre, client, date_de_debut, date_de_fin) VALUES (:chambre, :client, date_de_debut, date_de_fin)');
                $query->bindValue('chambre', $_POST['chambre'], PDO::PARAM_STR);
                $query->bindValue('client', $_POST['client'], PDO::PARAM_STR);
                $query->bindValue('date_de_debut', $_POST['date_de_debut'], PDO::PARAM_STR);
                $query->bindValue('date_de_fin', $_POST['date_de_fin'], PDO::PARAM_STR);
                $sth = $query->execute();
                var_dump($sth);
            } else {
                echo "Pour réserver une chambre il faut préciser les dates !";
			}
			$form_data = $request->getParsedBody();

			exit;
	}

	public function formLoc() {
		$view = new View('ajoutLocation');

		$view_data = [
			'html_title' => 'Ajout location',
			'html_h1' => 'Nouvelle location'
		];

		$view->render( $view_data );
	}

	public function ajoutLocation(ServerRequest $request): void
	{

		$form_data = $request->getParsedBody();

		// Si la variable "$_Post" contient des informations alors on les traitres
		$valid = true;

		if(empty($titre)){
			$valid = false;
			$er_titre = ("Le titre doit être renseigné");
		}

		if(empty($adresse)){
			$valid = false;
			$er_adresse = ("Non mais allo quoi ! T'es une loc et t'as pas d'adresse ?");
		}

		if(empty($prix)){
			$valid = false;
			$er_prix = "C'est moyennement fun de découvrir un prix surprise sur place (sur prise sur place ?)";
		}

		$annonceur = '';
		// Si toutes les conditions sont remplies alors on fait le traitement
		if($valid = true){
			$annonceur == $_SESSION['id'];
		}
/*
		if(isset($_POST['equip'])) {
			foreach($_POST['equip']) {

			}
		}
              if($valid){
	$query = Database::get()->prepare('INSERT INTO chambres (titre, adresse, prix, type, taille, description, couchage, annonceur) VALUES (:titre, :adresse, :prix, :type, :taille, :description, :couchage, :annonceur)');
	$query->bindValue('titre', $_POST['titre'], PDO::PARAM_STR);
	$query->bindValue('adresse', $_POST['adresse'], PDO::PARAM_STR);
	$query->bindValue('prix', $_POST['prix'], PDO::PARAM_STR);
	$query->bindValue('type', $_POST['type'], PDO::PARAM_STR);
	$query->bindValue('taille', $_POST['taille'], PDO::PARAM_STR);
	$query->bindValue('description', $_POST['description'], PDO::PARAM_STR);
	$query->bindValue('couchage', $_POST['couchage'], PDO::PARAM_STR);
	$query->bindValue('annonceur', $_POST['annonceur'], PDO::PARAM_STR);
	$sth = $query->execute();
	var_dump($sth);
}else {
	echo"Le formulaire d'ajout doit être complété correctement !";
}*/

		if($valid = true){
/*
			$query = Database::get()->prepare('INSERT INTO chambres (titre, adresse, prix, type, taille, description, couchage, annonceur) VALUES (:titre, :adresse, :prix, :type, :taille, :description, :couchage, :annonceur)');
			$query->bindValue('titre', $_POST['titre'], PDO::PARAM_STR);
			$query->bindValue('adresse', $_POST['adresse'], PDO::PARAM_STR);
			$query->bindValue('prix', $_POST['prix'], PDO::PARAM_STR);
			$query->bindValue('type', $_POST['type'], PDO::PARAM_STR);
			$query->bindValue('taille', $_POST['taille'], PDO::PARAM_STR);
			$query->bindValue('description', $_POST['description'], PDO::PARAM_STR);
			$query->bindValue('couchage', $_POST['couchage'], PDO::PARAM_STR);
			$query->bindValue('annonceur', $_POST['annonceur'], PDO::PARAM_STR);
			$sth = $query->execute();
*/
			$location = $this->rm->getUserRepo()->PostLocation($form_data);

			header('Location: /');

		} else {
			echo "L'ajout n'a pas fonctionné ! Vérifiez les champs !";

		}

		//header('Location: /');
		exit;

	}

}