<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;

class PageController extends Controller
{
	public function index(): void
	{
		$view = new View( 'home' );

		$view_data = [
			'html_title'	=> 'RBnB - acceuil',
			'html_h1'		=> '',
			'latest_chambres' => $this->rm->getChambresRepo()->findAll(),//["SELECT * FROM chambres ORDER BY id DESC LIMIT 12"],
			'chambres' => ["SELECT * FROM chambres"]
		];
		if (isset($_SESSION['id'])) {
		$user = $_SESSION['nom'].' '.$_SESSION['prenom'];
		$user_id = $_SESSION['id'];
		$user_role = $_SESSION['role'];
		$user_mail = $_SESSION['mail'];
		$user_tel = $_SESSION['tel'];
		}

		$view->render( $view_data );
	}
}