<?php

namespace App\Models;

use Core\IModel;
use Core\Model;

class User extends Model implements IModel
{
	public ?int $id;
	public string $nom;
	public string $prenom;
	public string $mdp;
	public string $mail;
	public string $tel;
	public ?int $role;
}