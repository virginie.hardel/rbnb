<?php

namespace App\Models;

use Core\IModel;
use Core\Model;

class Chambres extends Model implements IModel
{
    public string $titre;
    public string $adresse;
    public $prix;
    public int $type;
    public $taille;
    public string $description;
    public int $couchage;
    public int $annonceur;
}

