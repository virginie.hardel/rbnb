<?php

namespace App\Models;

use Core\IModel;
use Core\Model;

class Reservation extends Model implements IModel
{
	public string $date_debut;
    public string $date_fin;
    public string $chambre;
    public string $client;
}