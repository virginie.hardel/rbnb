<?php

namespace App\Models;

use Core\IModel;
use Core\Model;

class Post extends Model implements IModel
{
	public string $title;
	public string $date_debut;
	public string $date_fin;
	public string $chambre;
	public string $client;
}