        <div id="container">
            <!-- zone de connexion -->

            <form action="/connexion" method="POST">
                <h1>Connexion</h1>

                <label><b>Email : </b></label>
                <input type="text" placeholder="Entrer votre adresse mail" name="mail" required>

                <label><b>Mot de passe : </b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="mdp" required>

                <button type="submit" name="connexion" class="bg-primary text-white">Se connecter !</button>
                <a href="inscription">Pas encore membre ? Inscrivez-vous ici !</a>
                <?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                    if($err==1 || $err==2)
                        echo "<p style='color:red'>Utilisateur ou mot de passe incorrect</p>";
                }
                ?>
            </form>
        </div>