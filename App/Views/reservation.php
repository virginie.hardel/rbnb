<section class="page-section bg-light text-primary mb-0 text-xl-center" id="inscription">
    <h1 class="page-section-heading d-inline-block text-primary mb-3">RESERVATION</h1>
    <form method="post" novalidate>
        <div>
            <ul>
                <li>
                <label for="DateStart">&nbsp;&nbsp;Date d'arrivée  <span class="requis">*</span></label>
                <input type="hidden" id="DateStartISO" name="DateStartISO" value="<?php echo $DateStartISO; ?>" />
                <input class="datepicker fl-date" type="text" name="date_du_debut" id="DateStart" placeholder="Cliquer sur la vignette calendrier" style="width:272px;" readonly required value="<?php echo $date_de_debut; ?>"  />
                </li>
            </ul>
        </div>
        <div>
            <ul>
                <li>
                <label for="DateEnd">&nbsp;&nbsp;Date de départ  <span class="requis">*</span></label>
                <input type="hidden" id="DateEndISO" name="DateEndISO" value="<?php echo $DateEndISO; ?>" />
                <input class="datepicker fl-date" type="text" name="date_de_fin" id="DateEnd" placeholder="Cliquer sur la vignette calendrier" style="width:272px;" readonly required value="<?php echo $date_de_fin; ?>" />
                </li>
            </ul>
        </div>

        <button type="submit" name="reserver" class="bg-primary text-white">Réserver</button>
    </form>
</section>