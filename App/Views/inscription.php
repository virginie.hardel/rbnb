<section class="page-section bg-light text-primary mb-0 text-xl-center" id="inscription">
        <h1 class="page-section-heading d-inline-block text-primary mb-3">INSCRIPTION</h1>
        <form method="post" novalidate>
            <?php

                // S'il y a une erreur sur le nom alors on affiche
                if (isset($er_nom)):
                ?>
                    <div><?php echo $er_nom ?></div>
                <?php endif; ?>

            <p>Nom : </p>
                <input type="text" placeholder="Votre nom" name="nom" value="<?php if(isset($nom)){ echo $nom; }?>" required>   
                    <?php if(isset($er_prenom)): ?>
                            <div><?php echo  $er_prenom ?></div>
                    <?php endif; ?><br><br>

            <p>Prénom : </p>
                <input type="text" placeholder="Votre prénom" name="prenom" value="<?php if(isset($prenom)){ echo $prenom; }?>" required>   
                <?php
                    if (isset($er_mail)):
                ?>
                        <div><?php echo $er_mail ?></div>
                <?php endif; ?><br><br>

            <p>Email : </p>
                <input type="email" placeholder="Adresse mail" name="mail" value="<?php if(isset($mail)){ echo $mail; }?>" required>
                <?php
                    if (isset($er_mdp)){
                ?>
                 <div><?= $er_mdp ?></div>
                <?php } ?><br><br>

            <p>Mot de passe : </p>
                <input type="password" placeholder="Mot de passe" name="mdp" value="<?php if(isset($mdp)){ echo $mdp; }?>" required><br><br>
            <p>Confirmation du mot de passe : </p>
                <input type="password" placeholder="Confirmer le mot de passe" name="confmdp" value="<?php if(isset($confmdp)){ echo $confmdp; }?>" required> <br> <br>

            <p>Numéro de téléphone : </p>
                <input type="text" placeholder="Numéro de téléphone" name="tel" value="<?php if(isset($tel)){ echo $tel; }?>"> <br> <br>

            <label for="role">Vous voulez créer un compte utilisateur (pour faire des réservations) ou annonceur (pour proposer des logements et chambres) : </label> <br>
                <select name="role" classe="bg-secondary" id="role">
                    <option value="0">utilisateur</option>
                    <option value="1">annonceur</option>
                </select> <br> <br>

            <a href="/inscription"><button type="submit" class="bg-primary text-white">S'inscrire !</button></a>
        </form>
</section>
