<h1><?php echo $html_h1 ?></h1>

    <?php

       foreach($latest_chambres as $chambres): ?>
            <h2 class="portfolio-modal-title text-secondary mb-0"><strong><?php echo $chambres->titre ?></strong></h2>
            <ul>
                <li><?php echo 'Adresse : '.$chambres->adresse ?></li>
                <li><?php echo 'Prix à la nuit : '.$chambres->prix. ' €' ?></li>
                <li><?php echo 'Type de logement : ';
                    if($chambres->type == 0){
                        echo 'logement';
                    } elseif($chambres->type == 1){
                        echo 'chambre';
                    } else {
                        echo 'chambre à partager';
                    } ?></li>
                <li><?php echo 'Taille en m² : '.$chambres->taille ?></li>
                <li><?php echo 'Description : '.$chambres->description ?></li>
                <li><?php echo 'Nombre de couchages possible : '.$chambres->couchage ?></li>
                <?php /*
                if($equipements_chambre->chambre_id == $equipements_chambre->equipement_id){
                    if($equipements_chambre->equipement_id == $equipement->id) {
                        $cEquip = $equipement->label;
                    }
                } ?>
                <li><?php echo 'Equipements disponibles : '. $cEquip; */
                 ?></li>
            </ul>
            <?php if($_SESSION['role'] == 0) {?>
                 <a href="/reservation"><button id="reserver" type="button" class="bg-secondary" style="margin= 5px; padding=5px;">Réserver</button></a> <br>
            <?php }
            // TODO soyons sérieux, il faut modifier ce code mais je ne sais pas comment -> infos de session enregistrées en amont
                if($_SESSION['role'] == 1 && $_SESSION['id'] == $chambres->annonceur) : ?>
                <a href="/ajoutLocation"><button id="reserver" type="button" class="bg-secondary" style="margin= 5px; padding= 5px;">Modifier</button></a> <br>
                <?php endif;
            endforeach;
            if($_SESSION['role'] == 1) : ?>
            <br><a href="/ajoutLocation"><button id="reserver" type="button" class="bg-secondary" style="margin= 5px; padding= 5px;">Ajouter un nouveau logement</button></a> <br>
            <?php  endif;   ?>
                   </p>

        <section class="page-section bg-primary text-white mb-0" id="connexion">
            <div class="container">
                <!-- About Section Heading-->
                <div class="text-center">
                    <h2 class="page-section-heading d-inline-block text-white">DECONNEXION</h2>
                </div>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>

                <a href="App\Views\deconnexion.php" class="text-white justify-content-center"><button type="button" class="bg-secondary" style="margin= 5px; padding= 5px;">Au revoir ! Au revoir ! Président ! (se déconnecter)</button></a>
            </div>
        </section>
        <section class="page-section" id="contact">
            <div class="container">
                <!-- Contact Section Heading-->
                <div class="text-center">
                    <h2 class="page-section-heading text-secondary d-inline-block mb-0">CONTACT</h2>
                </div>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Contact Section Content-->
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="d-flex flex-column align-items-center">
                            <div class="icon-contact mb-3"><i class="fas fa-mobile-alt"></i></div>
                            <div class="text-muted">Phone</div>
                            <div class="lead font-weight-bold">(555) 555-5555</div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="d-flex flex-column align-items-center">
                            <div class="icon-contact mb-3"><i class="far fa-envelope"></i></div>
                            <div class="text-muted">Email</div><a class="lead font-weight-bold" href="mailto:name@example.com">name@example.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php // if($_SESSION['role'] == 0){
            echo '<a href=/userListe><button>Liste des utilisateurs du site et des réservations</button></a>';
        // }