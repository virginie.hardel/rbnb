<section class="page-section bg-light text-primary mb-0 text-xl-center" id="inscription">
        <h1 class="page-section-heading d-inline-block text-primary mb-3">AJOUT D'UNE LOCATION</h1>
        <form method="post" novalidate>
            <?php

                if (isset($er_titre)):
                ?>
                    <div><?php echo $er_titre ?></div>
                <?php endif; ?>

            <p>Titre de l'annonce : </p>
                <input type="text" placeholder="Votre titre" name="titre" value="<?php if(isset($titre)){ echo $titre; }?>" required>   
                    <?php if(isset($er_titre)): ?>
                            <div><?php echo  $er_titre ?></div>
                    <?php endif; ?><br><br>

            <p>Adresse du bien : </p>
                <input type="text" placeholder="Adresse" name="adresse" value="<?php if(isset($adresse)){ echo $adresse; }?>" required>   
                <?php
                    if (isset($er_adresse)):
                ?>
                        <div><?php echo $er_adresse ?></div>
                <?php endif; ?><br><br>

            <p>Prix à la nuit (montant en euros) : </p>
                <input type="text" placeholder="Prix à la nuit" name="prix" value="<?php if(isset($prix)){ echo $prix; }?>" required>
                <?php
                    if (isset($er_prix)){
                ?>
                 <div><?= $er_prix ?></div>
                <?php } ?><br><br>

            <p>Type de location proposée : </p>
            <select name="type" classe="bg-secondary" id="typeLocation">
                <option value="0">Chambre</option>
                <option value="1">Chambre partagée</option>
                <option value="2">logement entier</option>
            </select>

            <p>Taille de la location (en m2) : </p>
                <input type="text" placeholder="Taille" name="taille" value="<?php if(isset($taille)){ echo $taille; }?>"> <br> <br>

            <p>Description du bien : </p>
                <input type="text" placeholder="Description" name="description" value="<?php if(isset($description)){ echo $description; }?>"> <br> <br>

            <label for="couchage">Combien de couchages proposez-vous : </label> <br>
                <select name="choix_couchage" classe="bg-secondary" id="couchage">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                </select> <br> <br>

                <p>De quels équipements votre logement est équipé :</p>

                    <div>
                    <input type="checkbox" id="TV" name="equip" value="TV">
                    <label for="TV">TV</label>
                    </div>

                    <div>
                    <input type="checkbox" id="Bureau" name="equip" value="Bureau">
                    <label for="Bureau">Bureau</label>
                    </div>

                    <div>
                    <input type="checkbox" id="Clim" name="equip" value="Clim">
                    <label for="Clim">Clim</label>
                    </div>

                    <div>
                    <input type="checkbox" id="parking" name="equip" value="Place de Parking">
                    <label for="parking">Place de Parking</label>

                    <div>
                    <input type="checkbox" id="cuisine" name="equip" value="Cuisine équipée">
                    <label for="cuisine">Cuisine équipée</label>
                    </div>

                    <div>
                    <input type="checkbox" id="SdB" name="equip" value="Salle de bain">
                    <label for="SdB">Salle de bain</label>
                    </div>
                    </div>

                    <div>
                    <input type="checkbox" id="Accessoires" name="equip" value="Acessoires divers">
                    <label for="Accessoires">Accessoires divers</label>
                    </div>


            <button type="submit" name="ajoutLocation" class="bg-primary text-white">Ajouter la location !</button>

            <?php



            ?>
        </form>
</section>
