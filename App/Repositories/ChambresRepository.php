<?php

namespace App\Repositories;

use App\Models\Chambres;
use Core\Repository;

class ChambresRepository extends Repository
{
	public function getTable(): string
	{
		return 'chambres';
	}
	// CRUD
	// Read: Toute la liste
	public function findAll(): array
	{
		return $this->readAll( Chambres::class );
	}

	// Read: Une catégorie par son ID
	public function findById( int $id ): ?Chambres
	{
		return $this->readById( $id, Chambres::class );
	}
}