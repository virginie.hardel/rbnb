<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\Equipements;

class EquipementsRepository extends Repository
{
	public function getTable(): string
	{
		return 'equipements';
	}

	// CRUD
	// Read: Toute la liste
	public function findAll(): array
	{
		return $this->readAll( Equipements::class );
	}

	// Read: Un équipement par son ID
	public function findById( int $id ): ?Equipements
	{
		return $this->readById( $id, Equipements::class );
	}
}