<?php

namespace App\Repositories;

use \PDO;

use Core\Repository;
use App\Models\User;

class UserRepository extends Repository
{
	public function getTable(): string
	{
		return 'users';
	}

	// CRUD
	// Read: Toute la liste
	public function findAll(): array
	{
		return $this->readAll( User::class );
	}

	// Read: Un User par son ID
	public function findById( int $id ): ?User
	{
		return $this->readById( $id, User::class );
	}

	public function checkAuth( string $mail, string $mdp ): ?User
	{
		$query = sprintf(
			'SELECT * FROM %s WHERE mail=:mail AND mdp=:mdp',
			$this->getTable()
		);

		$sth = $this->db_cnx->prepare( $query );
		var_dump($sth);
		if( !$sth ) {
			return null;
		}

		// Attachement d'un paramètre avec précision de type
		$sth->bindValue( 'mail', $mail, PDO::PARAM_STR );
		$sth->bindValue( 'mdp', hash('sha512',$mdp), PDO::PARAM_STR );

		// Exécution de la requête préparée
		$sth->execute();

		// En cas d'erreur du serveur SQL on retourne null
		if( $sth->errorCode() !== PDO::ERR_NONE ) {
			return null;
		}

		$row = $sth->fetch();
		if( !$row ) {
			return null;
		}

		$user = new User( $row );
		$user->mdp = '';

		return $user;
	}

	public function PostLogin(array $users): ?User
	{
		$query = sprintf(
			"INSERT INTO %s (nom,prenom,mail,mdp,tel,role) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')",
			$this->getTable(),
			$users['nom'], $users['prenom'], $users['mail'], $users['mdp'], $users['tel'], $users['role']
		);

		$sth = $this->db_cnx->prepare( $query );
		var_dump($sth);
		if( !$sth ) {
			return null;
		}

		// Attachement d'un paramètre avec précision de type
/*
		$sth->bindValue('nom', $_POST['nom'], PDO::PARAM_STR);
		$sth->bindValue('prenom', $_POST['prenom'], PDO::PARAM_STR);
		$sth->bindValue('mail', $_POST['mail'], PDO::PARAM_STR);
		$sth->bindValue('mdp', $_POST['mdp'], PDO::PARAM_STR);
		$sth->bindValue('tel', $_POST['tel'], PDO::PARAM_STR); */
		// $sth->bindValue('role', $_POST['role'], PDO::PARAM_STR);

		// Exécution de la requête préparée
		$sth->execute();

		// En cas d'erreur du serveur SQL on retourne null
		if( $sth->errorCode() !== PDO::ERR_NONE ) {
			return null;
		}

		$row = $sth->fetch();
		if( !$row ) {
			return null;
		}

		$user = new User( $row );
		$user->mdp = '';

		return $user;

/*
		$query = Database::get()->prepare('INSERT INTO users (nom,prenom,mail,mdp,tel,role) VALUES (:nom, :prenom, :mail, :mdp, :tel, :role)');
		$query->bindValue('nom', $_POST['nom'], PDO::PARAM_STR);
		$query->bindValue('prenom', $_POST['prenom'], PDO::PARAM_STR);
		$query->bindValue('mail', $_POST['mail'], PDO::PARAM_STR);
		$query->bindValue('mdp', $_POST['mdp'], PDO::PARAM_STR);
		$query->bindValue('tel', $_POST['tel'], PDO::PARAM_STR);
		$query->bindValue('role', $_POST['role'], PDO::PARAM_STR);
		$sth = $query->execute(); */
	}
}