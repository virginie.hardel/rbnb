<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\Reservation;

class ReservationsRepository extends Repository
{
	public function getTable(): string
	{
		return 'reservation';
	}

	// CRUD
	// Read: Toute la liste
	public function findAll(): array
	{
		return $this->readAll( Reservation::class );
	}

	// Read: Une résa par son ID
	public function findById( int $id ): ?Reservation
	{
		return $this->readById( $id, Reservation::class );
	}
}